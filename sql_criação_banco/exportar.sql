--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Outubro-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence S_ID_ENDERECO
--------------------------------------------------------

   CREATE SEQUENCE  "CORONATEC"."S_ID_ENDERECO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table CONTA
--------------------------------------------------------

  CREATE TABLE "CORONATEC"."CONTA" 
   (	"NUMERO" NUMBER, 
	"SALDO" NUMBER, 
	"LIMITE" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table ENDERECO
--------------------------------------------------------

  CREATE TABLE "CORONATEC"."ENDERECO" 
   (	"RUA" VARCHAR2(200 BYTE), 
	"NUMERO" NUMBER, 
	"COMPLEMENTO" VARCHAR2(4 BYTE), 
	"ID_ENDERECO" NUMBER, 
	"CEP" VARCHAR2(14 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PESSOA
--------------------------------------------------------

  CREATE TABLE "CORONATEC"."PESSOA" 
   (	"CPF" VARCHAR2(14 BYTE), 
	"NOME" VARCHAR2(200 BYTE), 
	"SEXO" VARCHAR2(10 BYTE), 
	"IDADE" NUMBER, 
	"ID_ENDERECO" NUMBER, 
	"NUMERO_CONTA" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into CORONATEC.CONTA
SET DEFINE OFF;
Insert into CORONATEC.CONTA (NUMERO,SALDO,LIMITE) values ('11111','100','200');
Insert into CORONATEC.CONTA (NUMERO,SALDO,LIMITE) values ('22222','250','100');
Insert into CORONATEC.CONTA (NUMERO,SALDO,LIMITE) values ('888','999','999');
REM INSERTING into CORONATEC.CONTA_TESTE
SET DEFINE OFF;
REM INSERTING into CORONATEC.ENDERECO
SET DEFINE OFF;
Insert into CORONATEC.ENDERECO (RUA,NUMERO,COMPLEMENTO,ID_ENDERECO,CEP) values ('Rua','2','CS','30','013899');
REM INSERTING into CORONATEC.PESSOA
SET DEFINE OFF;
Insert into CORONATEC.PESSOA (CPF,NOME,SEXO,IDADE,ID_ENDERECO,NUMERO_CONTA) values ('8521465','blablabla','F','32','45','123');
--------------------------------------------------------
--  DDL for Index CONTA_NUMERO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORONATEC"."CONTA_NUMERO_PK" ON "CORONATEC"."CONTA" ("NUMERO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index ENDERECO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORONATEC"."ENDERECO_PK" ON "CORONATEC"."ENDERECO" ("ID_ENDERECO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PESSOA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORONATEC"."PESSOA_PK" ON "CORONATEC"."PESSOA" ("CPF") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CONTA_TESTE_NUMERO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORONATEC"."CONTA_TESTE_NUMERO_PK" ON "CORONATEC"."CONTA_TESTE" ("NUMERO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Trigger T_ID_ENDERECO
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CORONATEC"."T_ID_ENDERECO" 
BEFORE INSERT ON ENDERECO
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        IF :NEW.ID_ENDERECO IS NULL THEN
            SELECT S_ID_ENDERECO.NEXTVAL INTO :NEW.ID_ENDERECO FROM DUAL;
        END IF;
    END IF;
END;
/
ALTER TRIGGER "CORONATEC"."T_ID_ENDERECO" ENABLE;
--------------------------------------------------------
--  DDL for Procedure INSERE_CONTA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "CORONATEC"."INSERE_CONTA" (p_numero number, p_saldo number, p_limite number) As
  v_numero number;
  v_saldo number;
  v_limite number;
Begin
  v_numero := p_numero;
  v_saldo := p_saldo;
  v_limite := p_limite;

  Insert Into CONTA (numero, saldo, limite)
             Values (v_numero, v_saldo, v_limite);
  Commit;
End;

/
--------------------------------------------------------
--  DDL for Procedure INSERE_ENDERECO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "CORONATEC"."INSERE_ENDERECO" (p_rua varchar2, p_numero number, p_complemento varchar2, p_cep varchar2) As
  v_rua varchar2(200);
  v_numero number;
  v_complemento varchar2(4);
  
  v_id_endereco varchar2(200);
Begin
  v_rua := p_rua;
  v_numero := p_numero;
  v_complemento := p_complemento;

  Insert Into ENDERECO(id_endereco, rua, numero, complemento)
                Values((select s_id_endereco into ), v_rua, v_numero, v_complemento);

  Commit;

End;

/
--------------------------------------------------------
--  DDL for Procedure INSERE_PESSOA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "CORONATEC"."INSERE_PESSOA" (p_cpf varchar2, p_nome varchar2, p_sexo varchar2, p_idade number, p_id_endereco number, p_numero_conta number) As
  v_cpf varchar2(14);
  v_nome varchar2(200);
  v_sexo varchar2(10);
  v_idade number;
  v_id_endereco number;
  v_numero_conta number;
Begin
  v_cpf := p_cpf;
  v_nome := p_nome;
  v_sexo := p_sexo;
  v_idade := p_idade;
  v_id_endereco := p_id_endereco;
  v_numero_conta := p_numero_conta;

  Insert Into PESSOA(cpf, nome, sexo, idade, id_endereco, numero_conta)
              Values(v_cpf, v_nome, v_sexo, v_idade, v_id_endereco, v_numero_conta);

  Commit;
End;

/
--------------------------------------------------------
--  Constraints for Table ENDERECO
--------------------------------------------------------

  ALTER TABLE "CORONATEC"."ENDERECO" ADD CONSTRAINT "ENDERECO_PK" PRIMARY KEY ("ID_ENDERECO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CORONATEC"."ENDERECO" MODIFY ("ID_ENDERECO" NOT NULL ENABLE);
  ALTER TABLE "CORONATEC"."ENDERECO" MODIFY ("COMPLEMENTO" NOT NULL ENABLE);
  ALTER TABLE "CORONATEC"."ENDERECO" MODIFY ("NUMERO" NOT NULL ENABLE);
  ALTER TABLE "CORONATEC"."ENDERECO" MODIFY ("RUA" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CONTA
--------------------------------------------------------

  ALTER TABLE "CORONATEC"."CONTA" ADD CONSTRAINT "CONTA_NUMERO_PK" PRIMARY KEY ("NUMERO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table CONTA_TESTE
--------------------------------------------------------

  ALTER TABLE "CORONATEC"."CONTA_TESTE" ADD CONSTRAINT "CONTA_TESTE_NUMERO_PK" PRIMARY KEY ("NUMERO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CORONATEC"."CONTA_TESTE" MODIFY ("NUMERO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PESSOA
--------------------------------------------------------

  ALTER TABLE "CORONATEC"."PESSOA" ADD CONSTRAINT "PESSOA_PK" PRIMARY KEY ("CPF")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CORONATEC"."PESSOA" MODIFY ("IDADE" NOT NULL ENABLE);
  ALTER TABLE "CORONATEC"."PESSOA" MODIFY ("SEXO" NOT NULL ENABLE);
  ALTER TABLE "CORONATEC"."PESSOA" MODIFY ("NOME" NOT NULL ENABLE);
  ALTER TABLE "CORONATEC"."PESSOA" MODIFY ("CPF" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table PESSOA
--------------------------------------------------------

  ALTER TABLE "CORONATEC"."PESSOA" ADD CONSTRAINT "PESSOA_CONTA_NC_FK" FOREIGN KEY ("NUMERO_CONTA")
	  REFERENCES "CORONATEC"."CONTA" ("NUMERO") DISABLE;
  ALTER TABLE "CORONATEC"."PESSOA" ADD CONSTRAINT "PESSOA_ENDERECO_IE_FK" FOREIGN KEY ("ID_ENDERECO")
	  REFERENCES "CORONATEC"."ENDERECO" ("ID_ENDERECO") DISABLE;
