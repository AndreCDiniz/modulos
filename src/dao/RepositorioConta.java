package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidades.Conta;
import util.JDBCUtil;

public class RepositorioConta {

	public int inserirConta(Conta conta) {

		int qtdRow = 0;
		String sql = "INSERT INTO CONTA(NUMERO, SALDO, LIMITE)" + "VALUES (?,?,?)";

		Connection conexao;

		try {

			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setInt(1, conta.getNumero());
			ps.setDouble(2, conta.getSaldo());
			ps.setDouble(3, conta.getLimite());

			qtdRow = ps.executeUpdate();

			ps.close();
			conexao.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return qtdRow;

	}

	public int deletarConta(Conta conta) {

		int qtdRow = 0;

		String sql = "DELETE FROM CONTA WHERE NUMERO = ?";

		Connection conexao;

		try {

			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setInt(1, conta.getNumero());

			qtdRow = ps.executeUpdate();

			ps.close();
			conexao.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return qtdRow;

	}

	public int atualizarConta(Conta conta) {

		int qtdRow = 0;

		String sql = "UPDATE CONTA SET SALDO = ?, LIMITE = ? " + "WHERE NUMERO = ?";

		Connection conexao;

		try {

			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setDouble(1, conta.getSaldo());
			ps.setDouble(2, conta.getLimite());
			ps.setInt(3, conta.getNumero());

			qtdRow = ps.executeUpdate();

			ps.close();
			conexao.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return qtdRow;

	}

	public List<Conta> pesquisarConta(int numeroConta) {

		String sql = "select c.numero, c.saldo, c.limite " + " from Conta c ";

		if (numeroConta > 0) {
			sql += " where c.numero = ?";
		}

		List<Conta> retorno = new ArrayList<Conta>();

		Connection conexao;

		try {
			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			if (numeroConta > 0) {
				ps.setInt(1, numeroConta);
			} 

			ResultSet res = ps.executeQuery();

			while (res.next()) {

				Conta conta = new Conta();
				conta.setNumero(res.getInt("NUMERO"));
				conta.setSaldo(res.getDouble("SALDO"));
				conta.setLimite(res.getDouble("LIMITE"));

				retorno.add(conta);

			}

			ps.close();
			conexao.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retorno;

	}

}
