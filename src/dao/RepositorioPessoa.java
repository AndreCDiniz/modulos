package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidades.Pessoa;
import util.JDBCUtil;

public class RepositorioPessoa {// public class RepositorioPessoa

	public boolean inserirPessoa(Pessoa pessoa) {// public boolean inserirPessoa

		String sql = "insert into PESSOA (CPF" + ", NOME" + ", SEXO" + ", IDADE";
		if (pessoa.getEndereco().getIdEndereco() > 0)
			sql += ", ID_ENDERECO";
		if (pessoa.getConta().getNumero() > 0)
			sql += ", NUMERO_CONTA";
		sql += ") values (?" + ",?" + ",?" + ",?";
		if (pessoa.getEndereco().getIdEndereco() > 0)
			sql += ",?";
		if (pessoa.getConta().getNumero() > 0)
			sql += ",?";
		sql += ")";

		Connection conexao;
		try {
			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setString(1, pessoa.getCpf());
			ps.setString(2, pessoa.getNome());
			ps.setString(3, pessoa.getSexo());
			ps.setInt(4, pessoa.getIdade());
			if (pessoa.getConta().getNumero() > 0)
				ps.setInt(5, pessoa.getEndereco().getIdEndereco());
			if (pessoa.getEndereco().getIdEndereco() > 0)
				ps.setInt(6, pessoa.getConta().getNumero());

			ps.execute();
			ps.close();

			conexao.close();

			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}// public boolean inserirPessoa

	public boolean apagarPessoa(String cpf) {// public boolean apagarPessoa

		String sql = "delete from PESSOA where cpf = ?";

		Connection conexao;
		try {
			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setString(1, cpf);

			ps.executeUpdate();
			ps.close();

			conexao.close();

			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}// public boolean apagarPessoa

	public boolean atualizarPessoa(Pessoa pessoa) {

		String sql = "update PESSOA set NOME = ?, SEXO = ?, IDADE = ? where CPF = ?";

		Connection conexao;
		try {
			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setString(1, pessoa.getNome());
			ps.setString(2, pessoa.getSexo());
			ps.setInt(3, pessoa.getIdade());
			ps.setString(4, pessoa.getCpf());

			ps.executeUpdate();
			ps.close();

			conexao.close();

			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<Pessoa> pesquisarPessoa(String cpf) {

		String sql = "SELECT CPF, NOME, IDADE, SEXO FROM PESSOA ";
		
		if (cpf != null && !cpf.trim().isEmpty()) {
			sql += " where CPF = ? ";
		}
		
		List<Pessoa> retorno = new ArrayList<Pessoa>();

		Connection conexao;

		try {
			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			if (cpf != null && !cpf.trim().isEmpty()) {
				ps.setString(1, cpf);
			} 
				
			ResultSet res = ps.executeQuery();

			while (res.next()) {

				Pessoa p = new Pessoa();
				p.setCpf(res.getString("CPF"));
				p.setNome(res.getString("NOME"));
				p.setIdade(res.getInt("IDADE"));
				p.setSexo(res.getString("SEXO"));

				retorno.add(p);

			}

			ps.close();
			conexao.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retorno;

	}

}// public class RepositorioPessoa
