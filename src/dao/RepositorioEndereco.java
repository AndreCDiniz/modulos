package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidades.Endereco;
import util.JDBCUtil;

public class RepositorioEndereco {

	@SuppressWarnings("finally")
	public int inserirEndereco(Endereco endereco) {
		int qtdLinhas = 0;

		String sql = "Insert Into ENDERECO(Rua, Numero, Complemento, CEP) Values(?,?,?,?)";

		Connection conexao = JDBCUtil.getConexao();

		try {
			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setString(1, endereco.getRua());
			ps.setInt(2, endereco.getNumero());
			ps.setString(3, endereco.getComplemento());
			ps.setNString(4, endereco.getCep());

			qtdLinhas = ps.executeUpdate();

			ps.close();
			conexao.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return qtdLinhas;
		}

	}

	@SuppressWarnings("finally")
	public int deletarEndereco(Endereco endereco) {
		int qtdLinhas = 0;

		String sql = "Delete From ENDERECO Where numero = ?";

		Connection conexao = JDBCUtil.getConexao();

		try {
			PreparedStatement ps = conexao.prepareStatement(sql);

			ps.setInt(1, endereco.getNumero());

			qtdLinhas = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return qtdLinhas;
		}

	}

	@SuppressWarnings("finally")
	public int atualizarEndereco(Endereco endereco) {
		int qtdLinhas = 0;

		String sql = "Update ENDERECO Set Rua    	  = ?" + "					, Numero      = ?"
				+ "					, Complemento = ?" + "					, Cep		  = ?"
				+ "          	Where Id_Endereco = ?";

		Connection conexao = JDBCUtil.getConexao();

		PreparedStatement ps;
		try {
			ps = conexao.prepareStatement(sql);

			ps.setString(1, endereco.getRua());
			ps.setInt(2, endereco.getNumero());
			ps.setString(3, endereco.getComplemento());
			ps.setString(4, endereco.getCep());
			ps.setInt(5, endereco.getIdEndereco());

			qtdLinhas = ps.executeUpdate();

			ps.close();
			conexao.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return qtdLinhas;
		}
	}
	
	public List<Endereco> pesquisarEndereco(int idEndereco) {

		String sql = "SELECT ID_ENDERECO, RUA, NUMERO, COMPLEMENTO, CEP FROM ENDERECO";

		if (idEndereco > 0) {
			sql += " WHERE ID_ENDERECO = ? ";
		}

		List<Endereco> retorno = new ArrayList<Endereco>();

		Connection conexao;

		try {
			conexao = JDBCUtil.getConexao();

			PreparedStatement ps = conexao.prepareStatement(sql);

			if (idEndereco > 0) {
				ps.setInt(1, idEndereco);
			}

			ResultSet res = ps.executeQuery();

			while (res.next()) {

				Endereco e = new Endereco();
				
				e.setIdEndereco(res.getInt("ID_ENDERECO"));
				e.setRua(res.getString("RUA"));
				e.setNumero(res.getInt("NUMERO"));
				e.setComplemento(res.getString("COMPLEMENTO"));
				e.setCep(res.getString("CEP"));

				retorno.add(e);

			}

			ps.close();
			conexao.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retorno;
	}

}