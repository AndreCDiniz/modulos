package view;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dao.RepositorioConta;
import dao.RepositorioEndereco;
import dao.RepositorioPessoa;
import entidades.Conta;
import entidades.Endereco;
import entidades.Pessoa;

public class Tela {

	// Iniciados pelo metodo init que ser� usado no m�todo construtor
	RepositorioConta rConta;
	RepositorioEndereco rEndereco;
	RepositorioPessoa rPessoa;

	// Cada tipo possui seu scanner para evitar bugs
	// Scanner para n�meros
	Scanner scInt;
	// Scanner para Strings
	Scanner scStr;

	/**
	 * M�todo construtor que utiliza m�todo iniciaRecursos()
	 */
	public Tela() {
		this.iniciaRecursos();
	}

	public void gerarMenu() {

		int opcao = 0;

		System.out.println("\n                   =============================================");
		System.out.println("                  | 1 - Cadastrar Pessoa com Endere�o           |");
		System.out.println("                  | 2 - Alterar Pessoa e Endere�o               |");
		System.out.println("                  | 3 - Pesquisar Pessoa                        |");
		System.out.println("                  | 4 - Remover Pessoa e Endereco               |");
		System.out.println("                  | 5 - Listar todas as pessoas e seu endere�o  |");
		System.out.println("                  | 6 - Sair                                    |");
		System.out.println("                   =============================================\n");

		System.out.print("Op��o escolhida: ");
		opcao = scInt.nextInt();

		switch (opcao) {
		case 1:
			this.cadastrarPessoaEndereco();
			break;
		case 2:

			break;
		case 3:
			System.out.print("Informe o CPF para buscar: ");
			pesquisarPessoa(scStr.nextLine());
			break;
		case 4:
			
			break;
		case 5:

			break;
		case 6:

			System.exit(0);

		default:
			System.out.println("Op��o inexistente.");
		}

		this.gerarMenu();
	}

	/**
	 * Inicia recursos da classe para usar Deve ser chamado por qualquer metodo
	 * construtor para os m�todos funcionarem
	 */
	public void iniciaRecursos() {
		rPessoa = new RepositorioPessoa();
		rEndereco = new RepositorioEndereco();
		rConta = new RepositorioConta();

		scInt = new Scanner(System.in);
		scStr = new Scanner(System.in);
	}

	private void cadastrarPessoaEndereco() {
		// Inicia pessoa e atributos de obj
		Pessoa pessoa = new Pessoa();
		pessoa.setEndereco(new Endereco());
		pessoa.setConta(new Conta(0));

		System.out.print("Nome: ");
		pessoa.setNome(scStr.nextLine());

		System.out.print("Cpf: ");
		pessoa.setCpf(scStr.nextLine());

		System.out.print("Sexo: ");
		pessoa.setSexo(scStr.nextLine());

		System.out.print("Idade: ");
		pessoa.setIdade(scInt.nextInt());

		System.out.println("***Endereco***");

		System.out.print("Rua: ");
		pessoa.getEndereco().setRua(scStr.nextLine());

		System.out.print("N�mero: ");
		pessoa.getEndereco().setNumero(scInt.nextInt());

		System.out.print("Complemento: ");
		pessoa.getEndereco().setComplemento(scStr.nextLine());

		System.out.print("CEP: ");
		pessoa.getEndereco().setCep(scStr.nextLine());

		// Insere pessoa
		System.out.println(rPessoa.inserirPessoa(pessoa));
		// Insere endere�o
		System.out.println(rEndereco.inserirEndereco(pessoa.getEndereco()) + " endere�o cadastrado.");

	}

	/**
	 * Metodo utilizado para realizar a pesquisa no CASE 5
	 * @param cpf
	 */
	@SuppressWarnings("unused")
	private void pesquisarEndereco(int idEndereco) {
		List<Endereco> lista = new ArrayList<Endereco>();
		Endereco e = new Endereco();
		
		//Compara��o deve ser feita com .equals pois � tipo objeto.
		lista = rEndereco.pesquisarEndereco(idEndereco);
		if (idEndereco > 0) {
			for (Endereco endereco : lista) {
				if (endereco.getIdEndereco() == idEndereco) { 
					e = endereco;
				}
			}
			System.out.println(e);
		} else {
			for (Endereco endereco : lista) {
				System.out.println(endereco);
			}
		}
	}
	
	
	
	
	/**
	 * Metodo utilizado para realizar a pesquisa no CASE 3
	 * @param cpf
	 */
	private void pesquisarPessoa(String cpf) {
		List<Pessoa> lista = new ArrayList<Pessoa>();
		Pessoa p = new Pessoa();
		
		//Compara��o deve ser feita com .equals pois � tipo objeto.
		lista = rPessoa.pesquisarPessoa(cpf);
		if (cpf != null && !cpf.trim().isEmpty()) {
			for (Pessoa pessoa : lista) {
				if (pessoa.getCpf().equalsIgnoreCase(cpf)) { 
					p = pessoa;
				}
			}
			System.out.println(p);
		} else {
			for (Pessoa pessoa : lista) {
				System.out.println(pessoa);
			}
		}
	}
	
	/**
	 * Metodo para listar conta se necess�rio no menu..
	 * @param numero
	 */
	@SuppressWarnings("unused")
	private void listarConta(int numero) {

		List<Conta> contas = new ArrayList<Conta>();

		contas = rConta.pesquisarConta(numero);

		for (Conta conta : contas) {
			System.out.println(conta);
			;
		}

		System.out.println();
		System.out.println();
	}
}
