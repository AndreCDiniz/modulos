package teste;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TesteConexao {

	public static void main(String[] args) {
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conexao = 
			DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "CORONATEC","CORONATEC");
		
			String sql = "INSERT INTO ENDERECO (numero, rua, complemento) " + 
					"VALUES (79865, 'TESTE CONEXAO PROJETO', 'AP')";

			//Prepara a instru��o SQL
			PreparedStatement ps = conexao.prepareStatement(sql);
			
			//Executa a instru��o SQL
			ps.execute();
			
			ps.close();
			conexao.close();
			
			System.out.println("-- Inseriu --");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
