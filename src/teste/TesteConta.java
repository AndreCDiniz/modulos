package teste;
import java.util.List;
import java.util.Scanner;

import dao.RepositorioConta;
import entidades.Conta;

public class TesteConta {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		RepositorioConta repo = new RepositorioConta();

		List<Conta> contas = repo.pesquisarConta(sc.nextInt());

		for (Conta conta : contas) {
			System.out.println(conta.getNumero() + " - " + conta.getSaldo());
		}
		
		sc.close();
	}

}
