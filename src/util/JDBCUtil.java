package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtil {
	
	/**
	 * Metodo utilizado para recuperar uma conex�o com o banco de dados
	 * @return conex�o
	 */
		public static Connection getConexao() {
			
			Connection conexao = null;
			
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				conexao = 
					DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", 
							"CORONATEC","CORONATEC");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return conexao;
		}
		
	}
