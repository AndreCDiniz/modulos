package entidades;

public class Conta {
	
	private int numero;
	private double saldo;
	private double limite;
	
	/**
	 * Metodo construtor
	 */
	public Conta() {}

	public Conta(int numero) {
		this.numero = numero;
	}
	
	@Override
	public String toString() {
		
		String conta = "Conta - Dados"
					 + "\t- N�mero: " + this.getNumero() 
					 + "\t- Saldo: " + this.getSaldo() 
					 + "\t- Limite: " + this.getLimite();
		
		return conta;
	}
	
	//Getters & Setters
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLimite() {
		return limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	
	

}
