package entidades;

public class Endereco {
	private int idEndereco;
	private int numero;
	private String rua;
	private String complemento;
	private String cep;
	
	public Endereco(){}
	
	public Endereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	
	public Endereco(int numero, String rua, String complemento, String cep) {
		this.numero = numero;
		this.rua = rua;
		this.complemento = complemento;
		this.cep = cep;
	}
	
	public Endereco(int idEndereco, int numero, String rua, String complemento, String cep) {
		this.idEndereco  = idEndereco;
		this.numero 	 = numero;
		this.rua 		 = rua;
		this.complemento = complemento;
		this.cep 		 = cep;
	}
	
	@Override
	public String toString() {
		String dadosEndereco = "\n***Cadastro***\n"
				
							+ "\nRua:\t" 		 + 	this.rua 
							+ "\nNumero:\t"	 	 + 	this.numero
							+ "\nComplemento:\t" +	this.complemento
							+ "\nCEP:\t" 		 + 	this.cep;
		
		return dadosEndereco;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null || this.getClass() != obj.getClass())
			return false;
		
		Endereco e = (Endereco) obj;
		
		return this.rua 			 == e.getRua() 
		    && this.getNumero() 	 == e.getNumero() 
		    && this.getComplemento() == e.getComplemento()  
		    && this.getCep() 		 == e.getCep() 
		    && this.getIdEndereco()  == this.getIdEndereco();
 	}
	
	//Getters & Setters
	public int getIdEndereco() {
		return idEndereco;
	}
	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
}
