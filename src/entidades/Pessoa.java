package entidades;

public class Pessoa {
	
	private String cpf;
	private String nome;
	private String sexo;
	private int idade;
	private Endereco endereco;
	private Conta conta;
	
	/**
	 * M�todo construtor
	 */
	public Pessoa() {
	}
	
	/**
	 * M�todo construtor com todos os par�metros
	 * @param cpf
	 * @param nome
	 * @param sexo
	 * @param idade
	 * @param id_endereco
	 * @param numero_conta
	 */
	public Pessoa(String cpf, String nome, String sexo, int idade, Endereco endereco, Conta conta) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.sexo = sexo;
		this.idade = idade;
		this.endereco = endereco;
		this.conta = conta;
	}

	
	@Override
	public String toString() {
		
		String dadosPessoa = "\n***Cadastro***\n"
						+ "\nCpf:\t"   +  this.cpf	 
						+ "\nNome:\t"  +  this.nome
					 	+ "\nSexo:\t"  +  this.sexo
						+ "\nIdade:\t" +  this.idade;
		//if(this.endereco != null)
		
		return dadosPessoa;
	}
	
	//Getters & Setters
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public Endereco getEndereco() {
		return this.endereco;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public Conta getConta() {
		return this.conta;
	}
	
	public void setConta(Conta conta) {
		this.conta = conta;
	}

}
